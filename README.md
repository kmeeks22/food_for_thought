# README #

Food for thought is a start-up project that was started earlier in 2016. Turns out someone beat us to it. Still a fun project with some cool ideas. This portion of code was built more for demo purposes and is not necessarily complete or fully functional.

### Food for Thought ###

* Food for Thought is meant to be an uber-like application for personal food/cuisine  experiences.

### Tech Stack ###

* Google App Engine with Java and service endpoints
* Angular.js

### Most Recent Branch ###

* gEngine

### Operational Demo ###
* Follow: http://1-dot-food-for-thought-1223.appspot.com/index.html#/home
* Demo functionality allows users to search for options (for the demo, it does not matter what is entered into the fields), narrow options by price with price slider, click on the link to an option to see further detail and click on thumbnail images to display it larger in the image gallery.