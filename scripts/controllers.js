var findFoodControllers = angular.module('findFoodControllers', []);

findFoodControllers.controller('FindFoodForm', ['$scope',
	function($scope){
		$scope.findFood = function(){
			console.log("clicked: " + window.location.host +window.location.pathname +'#/dining_results/' + $scope.foodType + '/' + $scope.zip + '/' + $scope.delivery);
			window.location.href = 'http://'+ window.location.host + window.location.pathname + '#/dining_results/' + $scope.foodType + '/' + $scope.zip + '/' + $scope.delivery;
		};	
		
	}
]);

findFoodControllers.controller('FoodFinderResults', ['$scope', '$routeParams',
	function($scope, $routeParams){
		$scope.foodType = $routeParams.foodType;
		$scope.zip = $routeParams.zip;
		$scope.delivery = $routeParams.delivery;
		
		//TODO: Replace with service call
		$scope.diningResults = [
			{"id": "bb", "name": "Bob’s Burgers", "price": 10, "cuisine": "mexican", "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel nunc eget est auctor maximus fringilla elementum nibh. Morbi hendrerit posuere nisi id iaculis. Nulla." }, 
			{"id": "vv", "name": "Vinny’s Vineyard", "price": 35, "cuisine": "italian", "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel nunc eget est auctor maximus fringilla elementum nibh. Morbi hendrerit posuere nisi id iaculis. Nulla." }, 
			{"id": "jp", "name": "Jimmy Pestos", "price": 18,  "cuisine": "italian", "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel nunc eget est auctor maximus fringilla elementum nibh. Morbi hendrerit posuere nisi id iaculis. Nulla." }, 
			{"id": "rj", "name": "R & Js", "price": 150,  "cuisine": "mexican", "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel nunc eget est auctor maximus fringilla elementum nibh. Morbi hendrerit posuere nisi id iaculis. Nulla." }, 
		];
		
		// Initiate date picker
		$(function() {
		    $( "#reservation-date" ).datepicker({ 
		    	minDate: 0, 
		    	maxDate: "+1M +10D",
		    	showOn: "both",
		    	buttonImage: "img/calendar.png",
		    	buttonImageOnly: false,
		    	buttonText: ""
		    });
		});
		
		// Initiate slider
		$scope.priceRange = "$0 - $0";
		$(function() {
			var lowVal = 5, maxVal = 200;
		    $( "#price-range-slider" ).slider({
		      range: true,
		      min: 0,
		      max: 500,
		      values: [ 5, 200 ],
		      slide: function( event, ui ) {
		        $( "#price-range" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
		        $( "#price-range" ).trigger('change');
		      }
		    });
		    $scope.priceRange = "$" + lowVal + " - $" + maxVal;
		});
		
		
		
	}
]);

findFoodControllers.controller("DetailedListing", ["$scope", "$routeParams", "DiningDetails", 
	function($scope, $routeParams, DiningDetails){
		$scope.rid = $routeParams.rid;
		DiningDetails.get({appLoc: "appDat"}, function(details){
			$scope.details = details[$scope.rid];
			$scope.mainImage = $scope.details.images[0];
		});
		
		$scope.setImage = function(imgUrl){
			$scope.mainImage = imgUrl;
		};
		
		
	}
]);

