angular.module('foodFilters', [])
	.filter('filterRange', function() {
		return function( items, rangeInfo ) {
	        var filtered = [];
	        //console.log(rangeInfo);
	        var min = rangeInfo.split(" - $")[0].replace("$", "");
	        var max = rangeInfo.split(" - $")[1].replace("$", "");
	        // If time is with the range
	        angular.forEach(items, function(item) {
	            if( item.price >= min && item.price <= max ) {
	                filtered.push(item);
	            }
	        });
        return filtered;
	};
});