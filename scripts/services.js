var foodForThoughtServices = angular.module('foodForThoughtServices', ['ngResource']);

foodForThoughtServices.factory('DiningDetails', ["$resource", 
	function($resource){
		return $resource(':appLoc.json', {}, {
		});
	}
]);
