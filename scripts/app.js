var localFood = angular.module('localFood', ['ngRoute', 'findFoodControllers', 'foodFilters', 'foodForThoughtServices']); 

localFood.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.when('/home', {
    	templateUrl: 'templates/home.html',
    	controller: 'FindFoodForm'
    })
    .when('/dining_results/:foodType/:zip/:delivery', {
    	templateUrl: 'templates/dining_results.html',
    	controller: 'FoodFinderResults'
    })
    .when('/detailed_listing/:rid', {
    	templateUrl: 'templates/detailed_listing.html',
    	controller: 'DetailedListing'
    })
    .otherwise({
    	redirectTo: '/home' 
    });
  }  
]);

localFood.directive('mainNav', function(){
	return{
		restrict: 'E',
		templateUrl: 'templates/mainNav.html'
	};
});
